=======
sb_cols
=======

sb_cols is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds columns to page.


Installation
============

sb_cols requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_cols in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_cols


Configuration 
=============

1. Add "sb_cols" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_cols',
        ...
    )

2. Run `python manage.py migrate` to create the sb_cols models.  

