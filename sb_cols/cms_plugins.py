# from cms.constants import PLUGIN_MOVE_ACTION, PLUGIN_COPY_ACTION
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_cols.models import SBCols, SBCol
from sb_core.constants import BASE


class SBColsPlugin(CMSPluginBase):
    module = BASE
    render_template = 'sb_cols/sb_cols.html'
    name = 'Колонки'
    model = SBCols
    allow_children = True
    child_classes = ('SBColPlugin', )

    fieldsets = (
        ('', {
            'fields': ('title', 'is_align_by_height', 'use_media', 'is_reversed')
        }),
        ('Дополнительные настройки', {
            'classes': ('collapse',),
            'fields': ('attr_class', )
        })
    )

    def render(self, context, instance, placeholder):
        context = super(SBColsPlugin, self).render(context, instance, placeholder)

        i = 0
        children = instance.child_plugin_instances
        if instance.is_reversed:
            children = list(reversed(children))

        context['has_no_width_col'] = not all([child.width for child in children]) if children else False

        if context['has_no_width_col']:
            count = len(children)
            context['margin_style'] = ''
            for child in children:
                i += 1
                if not child.width:
                    context['index_no_width_col'] = i
                else:
                    if count == 2:
                        if i == 1:
                            context['margin_style'] = 'margin-left: {};'.format(child.width)
                        else:
                            context['margin_style'] += 'margin-right: {};'.format(child.width)
                    else:
                        if i == 1:
                            context['margin_style'] = 'margin-left: {};'.format(child.width)
                        else:
                            context['margin_style'] += 'margin-right: {};'.format(child.width)

        return context

plugin_pool.register_plugin(SBColsPlugin)


class SBColPlugin(CMSPluginBase):
    module = 'Колонки'
    render_template = 'sb_cols/sb_col.html'
    name = 'Колонка'
    model = SBCol
    allow_children = True
    parent_classes = ('SBColsPlugin', )

    # action_options = {
    #     PLUGIN_MOVE_ACTION: {
    #         'requires_reload': True
    #     },
    #     PLUGIN_COPY_ACTION: {
    #         'requires_reload': True
    #     },
    # }

    fieldsets = (
        ('', {
            'fields': ('width', )
        }),
        ('Дополнительные настройки', {
            'classes': ('collapse',),
            'fields': ('attr_class', )
        })
    )


plugin_pool.register_plugin(SBColPlugin)
