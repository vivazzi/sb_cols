/* SBL: sb_cols (sb_cols.js)
 * Copyright (c) 2013 - 2016 Maltsev Artem, maltsev.artjom@gmail.com, vivazzi.pro
 * web studio: Viva IT Studio, vits.pro
 */
(function($){
    $.fn.sb_cols = function(method) {
        var methods = {
            init : function() {
                this.each(function() {
                    function get_max_height($elements){
                        var max = $elements.eq(0).outerHeight();
                        $elements.each(function(index){
                            if (index > 0 && max < $(this).outerHeight()) max = $(this).outerHeight();
                        });
                        return max
                    }

                    function set_delay_height(){
                        // ugly hack for set height
                        var resize_intervals = [100, 200, 1000];
                        for (var i in resize_intervals){
                            setTimeout(function(){
                                $(window).resize();
                            }, resize_intervals[i]);
                        }
                    }

                    var $sb_cols = $(this);
                    var $col = $sb_cols.children();

                    /* --- Resize --- */
                    $(window).resize(function(){
                        $col.css({'height': 'auto'});
                        if (window.innerWidth > 767) $col.css({'height': get_max_height($col)});
                    }).resize();

                    set_delay_height();

                    window.addEventListener('orientationchange', function() {
                        $(window).resize();
                    });

                    $('.cms-toolbar-item-cms-mode-switcher').on('mousedown.hack_set_height', function(){
                        set_delay_height();
                    });
                });
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' +  method + '" is not exists for sb_cols');
        }
    };
})(jQuery);