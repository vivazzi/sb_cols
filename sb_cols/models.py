from django.core.exceptions import ValidationError
from django.db import models
from cms.models import CMSPlugin

from sb_core.constants import attr_class_ht, title_ht


class SBCols(CMSPlugin):
    title = models.CharField('Название', max_length=255, blank=True, help_text=title_ht)
    is_align_by_height = models.BooleanField('Выравнить по высоте?', default=False,
                                             help_text='Полезно, если колонки имеют какую-либо подложку')
    use_media = models.BooleanField('Выстраивать колонки друг под другом в мобильном отображении?', default=True)

    is_reversed = models.BooleanField('Выстроить колонки в мобильном отображении в обратном направлении?', default=False)

    attr_class = models.CharField('Класс', max_length=50, blank=True, help_text=attr_class_ht)

    def __str__(self):
        return self.title or ''

    class Meta:
        db_table = 'sb_cols'
        verbose_name = 'Контейнер колонок'
        verbose_name_plural = 'Контейнеры колонок'


class SBCol(CMSPlugin):
    ht = ('Измеряется в пикселях или в процентах. Например: 100px, 50%.<br/>'
          'Значение можно не указывать, если планируется разместить две колонки и одна из колонок имеет ширину в пикселях.<br>'
          'Это полезно при размещении фиксированной колонки.')

    width = models.CharField('Ширина колонки', max_length=20, blank=True, help_text=ht)

    attr_class = models.CharField('Класс', max_length=50, blank=True, help_text=attr_class_ht)

    def __str__(self):
        return self.width

    def save(self, no_signals=False, *args, **kwargs):
        self.width = self.width.replace(',', '.')
        super(SBCol, self).save(*args, **kwargs)

    # todo: clean don't validate
    def clean(self):
        parts = self.width.split()
        if len(parts) > 2:
            ValidationError('Ошибка ввода ширины колонки, проверьте правильность написания.')

        if len(parts) == 2:
            try:
                float(parts[0])
            except ValueError:
                ValidationError('Значение ширины колонки должно быть целое или дробное число.')

            if parts[1] not in ('px', '%'):
                ValidationError('Используйте либо пиксели (px), либо проценты (%) для значения ширины колонки')

        if len(parts) == 1:
            try:
                float(parts[0])
                self.width = '{}px'.format(parts[0])
            except ValueError:
                if parts[0][-1] == '%' or parts[0][-2:-1] == 'px':
                    try:
                        float(parts[0][0:-1])
                    except ValueError:
                        ValidationError('Значение ширины колонки должно быть целое или дробное число.')
                else:
                    ValidationError('Используйте либо пиксели (px), либо проценты (%) для значения ширины колонки')

    class Meta:
        db_table = 'sb_col'
        verbose_name = 'Колонка'
        verbose_name_plural = 'Колонки'
