from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBColsConfig(AppConfig):
    name = 'sb_cols'
    verbose_name = _('Columns')
