from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_cols', '0004_auto_20160219_0916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbcol',
            name='attr_class',
            field=models.CharField(help_text='\u0412\u0441\u043f\u043e\u043c\u043e\u0433\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043a\u043b\u0430\u0441\u0441 \u0434\u043b\u044f \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u0430', max_length=50, null=True, verbose_name='\u041a\u043b\u0430\u0441\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='sbcols',
            name='attr_class',
            field=models.CharField(help_text='\u0412\u0441\u043f\u043e\u043c\u043e\u0433\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043a\u043b\u0430\u0441\u0441 \u0434\u043b\u044f \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u0430', max_length=50, null=True, verbose_name='\u041a\u043b\u0430\u0441\u0441', blank=True),
        ),
    ]
