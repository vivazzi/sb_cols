from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20140926_2347'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBCol',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('width', models.CharField(help_text='\u0418\u0437\u043c\u0435\u0440\u044f\u0435\u0442\u0441\u044f \u0432 \u043f\u0438\u043a\u0441\u0435\u043b\u044f\u0445 \u0438\u043b\u0438 \u0432 \u043f\u0440\u043e\u0446\u0435\u043d\u0442\u0430\u0445. \u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: 10px, 50%. \u0415\u0441\u043b\u0438 \u0431\u0443\u0434\u0435\u0442 \u0443\u043a\u0430\u0437\u0430\u043d\u043e \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0431\u0435\u0437 \u043d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u044f (\u043d\u0430\u043f\u0440. 10), \u0442\u043e \u0431\u0443\u0434\u0435\u0442 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c\u0441\u044f \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0432 \u043f\u0438\u043a\u0441\u0435\u043b\u044f\u0445', max_length=20, verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430 \u043a\u043e\u043b\u043e\u043d\u043a\u0438')),
            ],
            options={
                'db_table': 'sb_col',
                'verbose_name': '\u041a\u043e\u043b\u043e\u043d\u043a\u0430',
                'verbose_name_plural': '\u041a\u043e\u043b\u043e\u043d\u043a\u0438',
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SBCols',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('title', models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u0438\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'sb_cols',
                'verbose_name': '\u041a\u043e\u043d\u0442\u0435\u0439\u043d\u0435\u0440 \u043a\u043e\u043b\u043e\u043d\u043e\u043a',
                'verbose_name_plural': '\u041a\u043e\u043d\u0442\u0435\u0439\u043d\u0435\u0440\u044b \u043a\u043e\u043b\u043e\u043d\u043e\u043a',
            },
            bases=('cms.cmsplugin',),
        ),
    ]
