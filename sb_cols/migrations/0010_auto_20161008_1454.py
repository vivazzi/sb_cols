from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_cols', '0009_auto_20160809_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbcol',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_cols_sbcol', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='sbcols',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_cols_sbcols', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
