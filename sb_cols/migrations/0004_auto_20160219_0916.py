from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_cols', '0003_auto_20150822_1933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbcols',
            name='title',
            field=models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
