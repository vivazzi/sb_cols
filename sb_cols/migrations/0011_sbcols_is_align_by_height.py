from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sb_cols', '0010_auto_20161008_1454'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbcols',
            name='is_align_by_height',
            field=models.BooleanField(default=False, help_text='\u041f\u043e\u043b\u0435\u0437\u043d\u043e, \u0435\u0441\u043b\u0438 \u043a\u043e\u043b\u043e\u043d\u043a\u0438 \u0438\u043c\u0435\u044e\u0442 \u043a\u0430\u043a\u0443\u044e-\u043b\u0438\u0431\u043e \u043f\u043e\u0434\u043b\u043e\u0436\u043a\u0443', verbose_name='\u0412\u044b\u0440\u0430\u0432\u043d\u0438\u0442\u044c \u043f\u043e \u0432\u044b\u0441\u043e\u0442\u0435?'),
        ),
    ]
