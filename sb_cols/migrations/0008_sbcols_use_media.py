from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_cols', '0007_auto_20160415_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbcols',
            name='use_media',
            field=models.BooleanField(default=True, verbose_name='\u0412\u044b\u0441\u0442\u0440\u0430\u0438\u0432\u0430\u0442\u044c \u043a\u043e\u043b\u043e\u043d\u043a\u0438 \u0434\u0440\u0443\u0433 \u043f\u043e\u0434 \u0434\u0440\u0443\u0433\u043e\u043c \u0432 \u043c\u043e\u0431\u0438\u043b\u044c\u043d\u043e\u043c \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0438?'),
        ),
    ]
